#ifndef ASSIGNMENT3_PIXELWIDGET_H
#define ASSIGNMENT3_PIXELWIDGET_H

#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <vector>
#include <bits/stdc++.h>

#include "PPMOutput.h"

using namespace std;

class PixelWidget {
public:
    PixelWidget(int triangleCoords[3][2], int colours[3][3], int background[3], float UV[3][2]);

    void DrawImage();

    bool HalfPlaneTest();

    void ReadPPM();

    void UVTextCoords();

    void TextInterpolation();

    void SetBackground();

private:
    int backgroundColour[3];
    int coords[3][2];
    int triangleColour[3][3];
    float triangleUV[3][2];
    float alpha;
    float beta;
    float gamma;
    int red;
    int green;
    int blue;
    int RGB[3]; //{R, G, B}
    int MIN = 0;
    int MAX = 255;

    string line;
    int inputRGB[3];
    int imageSize[2];

    int uvCoords[3][2];
    float textureX;
    float textureY;
    float denominator;
    vector<vector<tuple<int, int, int>>> RGBvals;
    vector<vector<tuple<int, int, int>>> inputRGBvals;

};


#endif
