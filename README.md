# Basic Rasterizer

Rasterizer that draws a triangle and then maps a segment of the texture from the input folder onto it.
After running the program the output will be stored in the output folder.

## Installation

In the same directory as the Makefile run

```bash
make
```

Then run

```bash
./a.out
```
