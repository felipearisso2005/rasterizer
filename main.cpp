#include "PixelWidget.h"

int main() {

    int triangleCoords[3][2] = {
            {61,  10}, // Coord A
            {100, 100}, // Coord B
            {25,  90} // Coord C
    };

    int triangleColour[3][3] = {
            {255, 0,   0}, // Coord A
            {0,   255, 0}, //Coord B
            {0,   0,   255} //Coord C
    };

    float triangleUV[3][2]{
            {0.160268, 0.290086},
            {0.083611, 0.159907},
            {0.230169, 0.222781}
    };

    int backgroundColour[3] = {255, 255, 192};

    PixelWidget triangle(triangleCoords, triangleColour, backgroundColour, triangleUV);

    triangle.ReadPPM();
    triangle.UVTextCoords();
    triangle.DrawImage();

    return 0;
}
